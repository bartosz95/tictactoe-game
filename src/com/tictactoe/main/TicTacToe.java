package com.tictactoe.main;

import java.awt.*;
import java.util.Random;
import java.util.Scanner;

public class TicTacToe extends Canvas implements Runnable {

    public static final Random RANDOM = new Random();

    int WIDTH=640,HEIGHT=480;
    private Thread thread;
    private boolean running = false;
    private Board board;
    private Scanner scanner;

    public TicTacToe(){
        board = new Board();
        scanner = new Scanner(System.in);
        new Window(WIDTH,HEIGHT,"Tic Tac Toe", this);
    }

    public synchronized void start(){
        thread = new Thread(this);
        thread.start();
        running = true;
    }

    public synchronized void stop(){
        try{
            thread.join();
            running = false;
        }catch (Exception e){
            e.printStackTrace();
        }

        thread = new Thread(this);
        thread.start();
    }

    public void run(){

        board.displayBoard();
        System.out.println("Select turn:\n1. Computer (X) / 2. User (O) : ");
        int choice = scanner.nextInt();

        if(choice == Board.PLAYER_X){
            Point p = new Point(RANDOM.nextInt(3), RANDOM.nextInt(3));
            board.placeMove(p, Board.PLAYER_X);
            board.displayBoard();
        }

        while(!board.isGameOver()) {
            boolean moveOk = true;

            do {
                if(!moveOk) System.out.println("Cell already filled !");

                System.out.println("Your move: ");
                Point userMove = new Point(scanner.nextInt(), scanner.nextInt());
                moveOk = board.placeMove(userMove, Board.PLAYER_O);

            }while(!moveOk);

            board.displayBoard();

            if (board.isGameOver()) break;

            board.minimax(0, Board.PLAYER_X);
            System.out.println("Computer choose position: " + board.computerMove);

            board.placeMove(board.computerMove, Board.PLAYER_X);
            board.displayBoard();
        }

        if(board.hasPlayerWon(Board.PLAYER_X)) System.out.println("You lost!");
        else if(board.hasPlayerWon(Board.PLAYER_O)) System.out.println("You win!");
        else System.out.println("Draw!");
    }

    public static void  main(String[] args) {
        new TicTacToe();
    }

}
