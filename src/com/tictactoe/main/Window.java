package com.tictactoe.main;

import javax.swing.*;
import java.awt.*;

public class Window extends Canvas {

    public Window(int width, int haight, String title, TicTacToe game){
        JFrame frame = new JFrame(title);

        frame.setPreferredSize(new Dimension(width, haight));
        frame.setMinimumSize(new Dimension(width, haight));
        frame.setMaximumSize(new Dimension(width, haight));

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.add(game);
        frame.setVisible(true);
        game.start();
    }

}
